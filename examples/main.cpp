﻿#include <cstdio>
#include <string.h>
#include <vector>
#include <iostream>
#include <time.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/time.h>
#include<sys/shm.h>
#include<sys/types.h>
#include<sys/mman.h>
#include<fcntl.h>
#include <stdint.h>

#include "../wx_log.h"
#include "../multi_hashmap.h"

int testfind(MultiHashMap  &shm) {

    auto v2 = shm.GetKeyStringValue((char*)"longkey4");
    logdebug("find value :%s", v2.c_str());
    logdebug("find value len:%d", v2.size());
    return 0;
}


class  TestTimeDura {

public:
    TestTimeDura() {
        gettimeofday(&start, NULL);
    }
    ~TestTimeDura() {
        gettimeofday(&end, NULL);
        during_time = end.tv_sec - start.tv_sec + (double)end.tv_usec / 1000000 - (double)start.tv_usec / 1000000;
        printf("test TestTimeDura  cost %lf sec\n", during_time);
    }
protected:
    struct timeval start;
    struct timeval end;
    double during_time;
};

//性能测试  qps 30万
int  bentechtest(MultiHashMap& shm) {
    //1千万
    wx_log_init(7, true, false);
    int successNum = 0;
    int failedNum = 0;
    int failedZeroNum = 0;
    TestTimeDura  t;
    for (int i = 0; i < 10000000; i++) {
        char buf[32 + 1] = { 0 };
        for (int i = 0; i < 32; i++) {
            int j = rand() % 10;
            buf[i] = j + 'a';
        }
        shm.SetKey(buf, buf);

        char outbuf[64] = { 0 };
        uint32_t  outValueLen = 0;
        int ret = shm.GetKeyWithBuf((uint8_t*)buf, strlen(buf), (uint8_t*)outbuf, 60, outValueLen);
        if (ret< 32  &&  ret>0) {
            logerror("failed GetKeyWithBuf");
            failedNum++;
        }else if(ret==0){
            failedZeroNum++;
        }
        else {
            successNum++;
        }
    }
    float f = (float)successNum*1.0 / (float)(successNum + failedNum);
    printf("bentechtest successNum:%d failedNum:%d,failedZeroNum:%d,pecent:%f\n", successNum, failedNum, failedZeroNum, f);
    return 0;
    //bentechtest successNum:1532224 failedNum:8467776
    //calCreateThisBlockNeedMemSize,outCellNum=3064450,totalHashNodesNum=1532225,cellSize=0,outKeyMemTotalSize=479586762,479(M),outValueMemTotalSize=0,0(M)
    //bentechtest successNum:9999657 failedNum:343,pecent:0.999966 test TestTimeDura  cost 43.731412 sec
}

//测试set 和查找
void testSetAndFind(MultiHashMap& shm) {

    char buf[6123 + 1] = { 0 };
    for (int i = 0; i < 6123; i++) {
        buf[i] = i % 10 + 'a';
    }

    shm.SetKey((char*)"longkey4", buf);

    //用户提供缓冲区进行读取查找
    uint32_t outValueLen = 0;
    char outbuf[8192] = { 0 };
    char key[128] = { "longkey4" };
    int len = shm.GetKeyWithBuf((uint8_t*)"longkey4", strlen(key), (uint8_t*)outbuf, 8190, outValueLen);
    logdebug("GetKeyWithBuf:[%d],%s", len, outbuf);


    auto  v2 = shm.GetKeyStringValue((char*)"longkey4");
    logdebug("find value :%s", v2.c_str());
    loginfo("find value len:%d", v2.size());
    //替換

    shm.SetKey((char*)"longkey4", "zxt");
    v2 = shm.GetKeyStringValue((char*)"longkey4");
    logdebug("find value :%s", v2.c_str());
    logdebug("find value len:%d", v2.size());


    shm.SetKey((char*)"longkey4", "replace3");
    v2 = shm.GetKeyStringValue((char*)"longkey4");
    logdebug("find value :%s", v2.c_str());
    logdebug("find value len:%d", v2.size());

    shm.SetKey((char*)"longkey4", "replace4");
    v2 = shm.GetKeyStringValue((char*)"longkey4");
    logdebug("find value :%s", v2.c_str());
    logdebug("find value len:%d", v2.size());

    shm.SetKey((char*)"longkey4", "replace5");
    v2 = shm.GetKeyStringValue((char*)"longkey4");
    logdebug("find value :%s", v2.c_str());
    logdebug("find value len:%d", v2.size());

}
int main() {
    printf("----teset--begin\n");
    wx_log_init(7, true, false);

   // system("ipcs  -m  |awk '{print \"ipcrm -m \"$2}'    |sh");

    uint32_t inHashFirstRowsNodeNum = 802224;
    uint32_t valueDataSize = 256;
    uint32_t inHashRowsNum = 15;

    MultiHashMap  shm;
    if (!shm.Create(0x124568, inHashFirstRowsNodeNum, valueDataSize, inHashRowsNum)) {
        LogError("FAILED Create2");
        return 0;
    }
    printf("shm total nodes num=%d\n", shm.GetTotalNodesNum());
    shm.Info();
    printf("----teset--begin\n");
    testSetAndFind(shm);
    bentechtest(shm);
    printf("----teset--end\n");
    
    return 0;
}                           