#ifndef  _shared_mutex_h_
#define _shared_mutex_h_


#include <sys/shm.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <sys/mman.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <pthread.h>

class   SharedMutex
{
public:
	SharedMutex();
	~SharedMutex();
public:
	int  create(bool new_flag);
	bool lock();
	void unlock();
protected:
	bool isInited();
	bool init();
protected:
	char               m_mutexBeginTag[16];
	unsigned long        m_flag;
	pthread_mutexattr_t  m_attr;
	pthread_mutex_t      m_mutex;
	char               m_mutexEndTag[16];
};

class MyShareAutoLock{
public:
	MyShareAutoLock(SharedMutex* mutex) :
		m_mutex(mutex){
		m_mutex->lock();
	}
	~MyShareAutoLock(){
		m_mutex->unlock();
	}
protected:
	SharedMutex* m_mutex;
};
void* getmemory(int shmkey, size_t  shmsize, int& new_falag);

#endif
