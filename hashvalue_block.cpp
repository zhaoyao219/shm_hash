#include "hashvalue_block.h"
#include <vector>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include "define.h"
#include "wx_log.h"


static inline
s_offset_list_t* offset_to_ptr(void* context, uint64_t  offset) {
	auto c = (HashValueBlock*)context;
	if (offset<c->GetCellStartOffset()) {
		if (offset != 0) {
			logerror("invalid offset:%u", offset);
		}
		return nullptr;
	}
	if (offset > c->GetCellEndOffset()) {
		if (offset != 0) {
			logerror("invalid offset:%u", offset);
		}
		return nullptr;
	}
	return (s_offset_list_t*)(c->GetBaseAddr() + offset);
}
static inline
uint64_t ptr_to_offset(void* context, s_offset_list_t* node) {
	auto c = (HashValueBlock*)context;
	return ((char*)node - c->GetBaseAddr());
}

HashValueBlock::HashValueBlock() {

	m_blockHead=nullptr;
	m_tranContext = nullptr;
	m_cellArray = nullptr;
	m_blockTail = nullptr;
}

HashValueBlock::~HashValueBlock() {

}

HashValueCell* HashValueBlock::getCell(uint32_t cellIndex) {

	uint64_t offset=m_blockHead->cellStartOffset + m_blockHead->cellSize * cellIndex;
	return   (HashValueCell*)((char*)m_blockHead + offset);
}
uint64_t HashValueBlock::getCellOffset(uint32_t cellIndex) {
	uint64_t offset = m_blockHead->cellStartOffset + m_blockHead->cellSize * cellIndex;
	return offset;
}
uint32_t  HashValueBlock::CELL_PTR_TO_INDEX(HashValueCell*  p) {
	int index=((char*)p - (char*)m_blockHead- m_blockHead->cellStartOffset) / m_blockHead->cellSize;
	return index;
}

inline
HashValueCell* HashValueBlock::CELL_OFFSET_TO_PTR(uint64_t offset) {
	if (offset > m_blockHead->cellEndOffset) {
		return nullptr;
	}
	if (offset < m_blockHead->cellStartOffset) {
		return nullptr;
	}
	return (HashValueCell*)((char*)m_blockHead + offset);
}
  
uint64_t HashValueBlock::GetCreateThisBlockNeedMemSize(const uint32_t  cellNum, const uint32_t  cellDataSize) {

	LogDebug("GetCreateThisBlockNeedMemSize cellNum=%d cellDataSize=%d", cellNum, cellDataSize);
	if (cellDataSize > HashBlockMaxCellDataSize) {
		return 0;
	}
	if (cellDataSize < HashBlockMinCellDataSize) {
		return 0;
	}
	uint32_t  cellsize = sizeof(HashValueCell) + cellDataSize;

	uint64_t  totalSize = 0;
	totalSize += sizeof(HashBlockHead);
	totalSize += sizeof(HashBlockTransactionContext);

	uint64_t celltotalsize = (uint64_t)cellsize * (uint64_t)cellNum;
	totalSize += celltotalsize;
	totalSize += sizeof(HashBlockTail);

	
	LogInfo("end GetCreateThisBlockNeedMemSize cellNum=%u cellDataSize=%u,size=%u(M),size=%u(G)", cellNum, cellDataSize, totalSize /1000000, totalSize / 1000000000);
	return totalSize;
}
bool HashValueBlock::CheckCellDataSize(const uint32_t  cellDataSize) {
	if (cellDataSize > HashBlockMaxCellDataSize) {
		return false;
	}
	if (cellDataSize < HashBlockMinCellDataSize) {
		return false;
	}
	return true;
}
void  HashValueBlock::initMemberAddress(char* addr, uint32_t  cellNum, uint32_t  cellDataSize) {

	logwarn("initMemberAddress,addr=%p cellNum=%d", addr, cellNum);
	char* pos = addr;
	m_blockHead = (HashBlockHead*)pos;

	pos += sizeof(HashBlockHead);
	m_tranContext = (HashBlockTransactionContext*)pos;

	pos += sizeof(HashBlockTransactionContext);
	m_cellArray = (HashValueCell*)pos;

	uint32_t cellSize = cellDataSize + sizeof(HashValueCell);
	uint64_t cellBytes= (uint64_t)cellSize * (uint64_t)cellNum;

	pos += cellBytes;
	m_blockTail = (HashBlockTail*)pos;

	logdebug("m_blockHead=%p,m_blockTail=%p", m_blockHead, m_blockTail);
	uint64_t len = (char*)m_blockTail - (char*)m_blockHead;
	logwarn("initMemberAddress pos=%ld", len);
	
	pos += sizeof(HashBlockTail);
	uint64_t  checklen = (char*)pos - (char*)addr;
	logdebug("checklen=%ld", checklen);
}
void  HashValueBlock::initHead(uint64_t  blockSize,uint32_t  cellNum , uint32_t  cellDataSize) {

	logwarn("initHead cellNum=%d", cellNum);
	memset(m_blockHead, 0, sizeof(HashBlockHead));

	//版本号
	strcpy(m_blockHead->magicID, MhtHead_MAGIC);
	m_blockHead->versionNo = HashBlockHead_VERSION_NO;
	//头大小
	m_blockHead->blockHeadSize = sizeof(HashBlockHead);
	m_blockHead->blockTailSize= sizeof(HashBlockTail);
	m_blockHead->tranHeadSize = sizeof(HashBlockTransactionContext);
	//整个内存的大小
	m_blockHead->cellNum = cellNum;
    m_blockHead->cellHeadSize = sizeof(HashValueCell);
    m_blockHead->cellSize = cellDataSize + sizeof(HashValueCell);
	uint64_t  cellToatlSize = (uint64_t)m_blockHead->cellSize * (uint64_t)m_blockHead->cellNum;
	m_blockHead->cellToatlSize = cellToatlSize;
	
    m_blockHead->cellDataSize = cellDataSize;
    m_blockHead->blockSize = blockSize;

    uint32_t  pos = sizeof(HashBlockHead) + sizeof(HashBlockTransactionContext);
    m_blockHead->cellStartOffset = pos;
    m_blockHead->cellEndOffset = (uint64_t)pos + (uint64_t)m_blockHead->cellSize * (uint64_t)cellNum;
    m_blockHead->createTime = time(NULL);
}
bool  HashValueBlock::checkHead() {

	if (strcmp(m_blockHead->magicID, MhtHead_MAGIC) != 0) {
		logerror("magicID not match");
		return false;
	}
	if (m_blockHead->versionNo != HashBlockHead_VERSION_NO) {
		logerror("versionNo not match");
		return false;
	}
	if (m_blockHead->blockHeadSize != sizeof(HashBlockHead)) {
		logerror("blockHeadSize not match");
		return false;
	}
	if (m_blockHead->blockTailSize != sizeof(HashBlockTail)) {
		logerror("blockTailSize not match");
		return false;
	}
	if (m_blockHead->tranHeadSize != sizeof(HashBlockTransactionContext)) {
		logerror("tranHeadSize not match");
		return false;
	}
	//-----------------------------------------------------

	if (m_blockHead->cellHeadSize != sizeof(HashValueCell)) {
		logerror("cellHeadSize not match");
		return false;
	}
	if (m_blockHead->cellDataSize > HashBlockMaxCellDataSize) {
		logerror("cellDataSize not match");
		return false;
	}
	if (m_blockHead->cellDataSize < HashBlockMinCellDataSize) {
		logerror("cellDataSize not match");
		return false;
	}
	uint32_t cellSize = m_blockHead->cellHeadSize + m_blockHead->cellDataSize;
	if (m_blockHead->cellSize != cellSize) {
		logerror("cellSize not match,m_blockHead->cellSize:%d,%d", m_blockHead->cellSize, cellSize);
		return false;
	}
	uint64_t  cellToatlSize = (uint64_t)m_blockHead->cellSize * (uint64_t)m_blockHead->cellNum;
	if (m_blockHead->cellToatlSize != cellToatlSize) {
		logerror("cellToatlSize not match");
		return false;
	}
	//---------------------------------------------------
	uint64_t blockSize = m_blockHead->blockHeadSize + m_blockHead->tranHeadSize + cellToatlSize + m_blockHead->blockTailSize;

	if (m_blockHead->blockSize != blockSize) {
		logerror("blockSize[%lu] not match[%lu]", m_blockHead->blockSize , blockSize);
		return false;
	}
	uint64_t cellStartOffset =m_blockHead->blockHeadSize + m_blockHead->tranHeadSize;
	if (m_blockHead->cellStartOffset != cellStartOffset) {
		logerror("cellStartOffset not match");
		return false;
	}
	uint64_t cellEndOffset = cellStartOffset + cellToatlSize;
	if (m_blockHead->cellEndOffset != cellEndOffset) {
		logerror("cellEndOffset[%lu] not match cellEndOffset[%lu]", m_blockHead->cellEndOffset , cellEndOffset);
		return false;
	}
	//-----------------------------------------------------
	if (strcmp(m_blockHead->blockInitTag, HashBlockInitTag) != 0) {
		logerror("blockInitTag not match");
		return false;
	}
	return true;
}

void  HashValueBlock::initTranContext() {
	memset((char*)m_tranContext, 0, sizeof(HashBlockTransactionContext));
}
bool  HashValueBlock::checkTranContext() {
	if (m_tranContext->status > HBTC_ACK_DELETE_VALUE) {
		return false;
	}
	if (m_tranContext->status < HBTC_IDLE) {
		return false;
	}
	return true;
}
void  HashValueBlock::initCellArray() {
}
bool  HashValueBlock::checkCellArray() {
	return true;
}
void  HashValueBlock::initTail() {

	logdebug("m_blockHead=%p,m_blockTail=%p", m_blockHead, m_blockTail);
	uint64_t len = (char*)m_blockTail-(char*)m_blockHead;
	logwarn("initMemberAddress pos=%ld", len);
	strcpy(m_blockTail->magicID, HashBlockTail_MAGIC);

}
bool  HashValueBlock::checkTail() {

	if (strcmp(m_blockTail->magicID, HashBlockTail_MAGIC) != 0){
	   logerror("HashBlockTail_MAGIC[%s]  not match", m_blockTail->magicID);
	   return false;
    }
	return true;
}

void  HashValueBlock::initAll(uint64_t  blockSize, uint32_t  cellNum , uint32_t  cellDataSize ) {

	if (strcmp(m_blockHead->blockInitTag, HashBlockInitTag) == 0) {
		//已经初始化了
		loginfo("HashValueBlock have  init");
		return;
	}
	initHead(blockSize, cellNum, cellDataSize);
	initTranContext();
	initCellArray();
	initTail();
	strcpy(m_blockHead->blockInitTag, HashBlockInitTag);
}

bool  HashValueBlock::checkAll() {
	if (!checkHead()) {
		logerror("failed checkHead");
		return false;
	}
	if (!checkTranContext()) {
		logerror("failed checkTranContext");
		return false;
	}
	if (!checkCellArray()) {
		logerror("failed checkCellArray");
		return false;
	}
	if (!checkTail()) {
		logerror("failed checkTail");
		return false;
	}
	logdebug("checkAll pass");
	return true;
}

bool   HashValueBlock::Create(char* addr, uint64_t  blockSize, const uint32_t  cellNum , const uint32_t  cellDataSize ) {

	loginfo("begin Create blockSize=%d cellNum=%d cellDataSize=%d", blockSize, cellNum, cellDataSize);
	if(cellDataSize > HashBlockMaxCellDataSize) {
		return false;
	}
	if (cellDataSize < HashBlockMinCellDataSize) {
		return false;
	}

	uint64_t  needSize=HashValueBlock::GetCreateThisBlockNeedMemSize(cellNum, cellDataSize);
	if (needSize != blockSize) {
		return false;
	}
	initMemberAddress(addr, cellNum, cellDataSize);
	if (strcmp(m_blockHead->blockInitTag, HashBlockInitTag) == 0) {
		//已经初始化
		if (!checkAll()) {
			return false;
		}
		else{
			loginfo("checkAll pass");
		}
	}
	else {
		//还没初始化，进行初始化
		initAll(blockSize, cellNum, cellDataSize);
	}

	return true;
}

void HashValueBlock::setCellReleasedStatus(HashValueCell* cell) {

	cell->lengthInCell = 0;
	cell->ValueSeq = 0;
	cell->cellStatus = HVCS_RELEASED;
}

void HashValueBlock::addIdleCellToFreeList(int count) {
	
	logdebug("addIdleCellToFreeList count:%d firstTimeAllocCellIndex=%d,m_blockHead->cellNum=%d",count, m_tranContext->firstTimeAllocCellIndex,m_blockHead->cellNum);
	if (count < 3) {
		count = 3;
	}
	for (int i = 0; i < count; i++) {
		if (m_tranContext->firstTimeAllocCellIndex >= m_blockHead->cellNum) {
			return;
		}
		HashValueCell* cell = getCell(m_tranContext->firstTimeAllocCellIndex);
		cell->cellStatus = HVCS_IDLE;
		m_tranContext->m_curOpCell = cell;
		setCellReleasedStatus(cell);
		s_offset_list_insert_head(&(m_tranContext->freeCellList), (s_offset_list_t*)cell, this, offset_to_ptr, ptr_to_offset);
		m_tranContext->firstTimeAllocCellIndex += 1;	
		m_tranContext->m_curOpCell = nullptr;
	}
	logdebug("end addIdleCellToFreeList firstTimeAllocCellIndex=%d,m_blockHead->cellNum=%d", m_tranContext->firstTimeAllocCellIndex, m_blockHead->cellNum);
}

void HashValueBlock::addReleasedCellToPreAllocList(HashValueCell* cell, uint64_t keySeq) {
	
	m_tranContext->m_curOpCell = cell;
	cell->lengthInCell = 0;
	cell->ValueSeq = keySeq;
	cell->cellStatus = HVCS_PRE_ALLOCED;
	s_offset_list_remove_head_next(&(m_tranContext->freeCellList), cell->next, this,offset_to_ptr, ptr_to_offset);
	s_offset_list_insert_head(&(m_tranContext->allocedList), (s_offset_list_t*)cell, this, offset_to_ptr, ptr_to_offset);
	m_tranContext->m_curOpCell = nullptr;

}

void HashValueBlock::addPreAllocCellToReleaseList(HashValueCell* cell) {
	
	m_tranContext->m_curOpCell = cell;
	//auto offset = CELL_PTR_TO_OFFSET(cell);
	cell->cellStatus = 0;
	cell->ValueSeq = 0;
	cell->cellStatus = HVCS_RELEASED;
	s_offset_list_remove_head(&(m_tranContext->allocedList), this, offset_to_ptr, ptr_to_offset);
	s_offset_list_insert_head(&(m_tranContext->freeCellList), (s_offset_list_t*)cell, this, offset_to_ptr, ptr_to_offset);
	m_tranContext->m_curOpCell = nullptr;
}

//分配内存
uint64_t   HashValueBlock::allocValueMemory(uint8_t* value, uint32_t valueLen, uint64_t keySeq) {
	int count = valueLen/m_blockHead->cellDataSize;
	if (valueLen % m_blockHead->cellDataSize!= 0) {
		count += 1;
	}
	int allocCount = 0;

	logdebug("allocValueMemory count=%d", count);
	m_tranContext->status = HBTC_addIdleCellToFreeList;
	addIdleCellToFreeList(count);
	m_tranContext->status = HBTC_IDLE;

	//从空闲CELL 里分配
	if (s_offset_list_empty(&(m_tranContext->freeCellList))) {
		logwarn("freeCellList is empty");
		return 0;
	}

	s_offset_list_t* head = s_offset_list_get_head(&(m_tranContext->freeCellList), this, offset_to_ptr, ptr_to_offset);
	while (head != nullptr) {
		allocCount++;
		if (allocCount >= count) {
			break;
		}
		head = s_offset_list_get_head(head, this, offset_to_ptr, ptr_to_offset);
	}
	if (allocCount < count) {
		//不能满足分配要求的cell数
		logwarn("allocCount[%d]<count[%d]", allocCount ,count);
		return 0;
	}

	allocCount = 0;
	m_tranContext->status = HBTC_addReleasedCellToPreAllocList;
	while(allocCount< count) {
		s_offset_list_t* head = s_offset_list_get_head(&(m_tranContext->freeCellList), this, offset_to_ptr, ptr_to_offset);
		if (head == nullptr) {
			break;
		}
		//可以分配
		HashValueCell* cell = (HashValueCell*)head;
		addReleasedCellToPreAllocList(cell, keySeq);
		allocCount++;
	}
	//当前的事务状态是等待写值
	m_tranContext->status = HBTC_WAIT_WRITE_VALUE;
	return m_tranContext->allocedList.next;
}
bool   HashValueBlock::writeValue(uint64_t offset,uint8_t* value, uint32_t valueLen, uint64_t keySeq) {

	uint8_t* src = value;
	uint32_t len = 0;
	
    s_offset_list_t* head = s_offset_list_get_head(&(m_tranContext->allocedList), this, offset_to_ptr, ptr_to_offset);
	HashValueCell* headCell = (HashValueCell*)head;
	HashValueCell* tailCell = headCell;

	while (head != nullptr) {
		HashValueCell* cell = (HashValueCell*)head;
		tailCell = cell;
		if (cell->ValueSeq != keySeq) {
			return false;
		}

		uint32_t  maxCopyLen = m_blockHead->cellDataSize;
		if (len + maxCopyLen > valueLen) {
			maxCopyLen = valueLen - len;
		}
		memcpy(cell->bufData, src, maxCopyLen);
		src += maxCopyLen;
		len += maxCopyLen;
		cell->lengthInCell = maxCopyLen;
		cell->cellStatus = HVCS_WRITED_VALUE;
	
		if (len >= valueLen) {
			break;
		}
		head = s_offset_list_get_next(head, this, offset_to_ptr, ptr_to_offset);
	}
	if (len != valueLen) {
		return false;
	}
	headCell->lengthInCell = valueLen;
	if (headCell != tailCell) {
		tailCell->valHead.next = offset;
	}
	if (tailCell->next != 0) {
		//实际分配的节点比需要的多
		return false;
	}
	return true;
}
uint64_t   HashValueBlock::ReqCreateValue(uint8_t* value, uint32_t valueLen, uint64_t keySeq) {

	
	logdebug("ReqCreateValue:firstTimeAllocCellIndex=%d,m_blockHead->cellNum=%u,valueLen=%d", m_tranContext->firstTimeAllocCellIndex, m_blockHead->cellNum, valueLen);

	if (m_tranContext->status != HBTC_IDLE && 
		m_tranContext->status != HBTC_ACK_USED_VALUE &&
		m_tranContext->status != HBTC_ACK_DELETE_VALUE) {
	    //当前事务状态不是终点，目前请求开启新事务，需要回滚当前事务
		rollbackCurTransactionContext();
	}

	uint64_t  valueID= allocValueMemory(value, valueLen, keySeq);
	if (valueID == 0) {
		return 0;
	}
	logdebug("allocValueMemory valueID=%u", valueID);
	if (!writeValue(valueID,value, valueLen, keySeq)) {
		//写值失败，回滚
		rollbackCurTransactionContext();
		return 0;
	}
	//分配成功,等待对方回应
	m_tranContext->status = HBTC_WAIT_WRITE_ACK;
	return valueID;
}
uint64_t   HashValueBlock::ReqAckUsedValue(uint64_t valueID, uint32_t valueLen, uint64_t valueSeq) {

	if (m_tranContext->status != HBTC_WAIT_WRITE_ACK &&
		m_tranContext->status != HBTC_ACK_USED_VALUE) {
		rollbackCurTransactionContext();
	}

	//HBTC_IDLE
	HashValueCell* cell = (HashValueCell*)(CELL_OFFSET_TO_PTR(valueID));
	HashValueCell* head = cell;

	if (cell == nullptr) {
		return REQ_VAL_ID_NOT_EXITED;
	}
	if (head->cellStatus == HVCS_SETED_OK) {
		//重复的确认
		if (m_tranContext->status == HBTC_WAIT_WRITE_ACK) {
			m_tranContext->status = HBTC_ACK_USED_VALUE;
		}
		return  0;
	}
	if (m_tranContext->status != HBTC_WAIT_WRITE_ACK) {
		return REQ_TR_STATUS_NOT_MATCH;
	}
	if (m_tranContext->allocedList.next != valueID) {
		//应该是allocedList 的首节点
		logerror("REQ_VAL_ID_NOT_MATCH");
		return REQ_VAL_ID_NOT_MATCH;
	}
	if (head->lengthInCell != valueLen) {
		//校验长度不一致
		logerror("REQ_VAL_LEN_NOT_MATCH");
		return REQ_VAL_LEN_NOT_MATCH;
	}
	if (head->ValueSeq != valueSeq) {
		return REQ_VAL_SEQ_NOT_MATCH;
	}
	m_tranContext->m_curOpCell = nullptr;
	m_tranContext->m_curOpCell2 = nullptr;

	//校验其他节点
	uint32_t  len = m_blockHead->cellDataSize;
	if (len > head->lengthInCell) {
		len = head->lengthInCell;
	}
	HashValueCell* tailCell = head;
	s_offset_list_t* cur = s_offset_list_get_next((s_offset_list_t*)head, this, offset_to_ptr, ptr_to_offset);
	while (cur != nullptr) {
		HashValueCell* cell = (HashValueCell*)cur;
		tailCell = cell;

		if (cell->cellStatus != HVCS_WRITED_VALUE) {
			logerror("REQ_VAL_STATUS_NOT_MATCH");
			return REQ_VAL_STATUS_NOT_MATCH;
		}
		if (cell->ValueSeq != valueSeq) {
			//不匹配
			logerror("REQ_VAL_SEQ_NOT_MATCH");
			return REQ_VAL_SEQ_NOT_MATCH;
		}
		len += cell->lengthInCell;
		if (len > valueLen) {
			logerror("REQ_VAL_LEN_NOT_MATCH,len=%d>valueLen=%d", len ,valueLen);
			return REQ_VAL_LEN_NOT_MATCH;
		}
		cur = s_offset_list_get_next((s_offset_list_t*)cur, this, offset_to_ptr, ptr_to_offset);
	}

	if (len != valueLen) {
		logerror("REQ_VAL_LEN_NOT_MATCH");
		return REQ_VAL_LEN_NOT_MATCH;
	}
	if (tailCell->next != 0) {
		//原allocedList 里还有数据,
		logerror("REQ_VAL_LEN_NOT_MATCH");
		return REQ_VAL_LEN_NOT_MATCH;
	}

	//校验一致可以操作,从队列里移除
	m_tranContext->m_curOpCell2 = head;
	m_tranContext->m_curOpCell = head;
	head->cellStatus = HVCS_SETED_OK;

	//首尾链表节点从原链表断裂出来
	s_offset_list_remove_head(&(m_tranContext->allocedList), this, offset_to_ptr, ptr_to_offset);
	s_offset_list_clear(&(m_tranContext->allocedList));
	
	head->valHead.next = valueID;
	tailCell->next = 0;
	tailCell->valHead.next = valueID;

	m_tranContext->m_curOpCell = nullptr;
	m_tranContext->m_curOpCell2 = nullptr;
	m_tranContext->status = HBTC_ACK_USED_VALUE;

	return 0;
}

//读取value数据(读线程)不改变数据，不参与状态机
int  HashValueBlock::ReqReadValue(uint64_t valueID, uint64_t valueSeq, uint32_t outValueLen, uint8_t* outValueP, uint32_t outBufSize){

	HashValueCell* cell = (HashValueCell*)(CELL_OFFSET_TO_PTR(valueID));
	HashValueCell* head = cell;
	if (cell == nullptr) {
		return REQ_VAL_ID_NOT_EXITED;
	}
	if (head->cellStatus != HVCS_SETED_OK) {
		//不是首cell
		return REQ_VAL_ID_NOT_EXITED;
	}
	if (head->lengthInCell != outValueLen) {
		//首cell记录value总长,其余cell记录实际长度
		return REQ_VAL_LEN_NOT_MATCH;
	}

	uint32_t  minBufSize = outValueLen;
	uint32_t  len = 0;
	uint8_t*  dest = outValueP;

	if (minBufSize > outBufSize) {
		minBufSize = outBufSize;
	}
	while (cell!=nullptr) {
		if (cell->cellStatus < HVCS_WRITED_VALUE) {
			return REQ_VAL_ID_NOT_EXITED;
		}
		if (cell->ValueSeq != valueSeq) {
			return REQ_VAL_SEQ_NOT_MATCH;
		}
		if (cell->lengthInCell == 0) {
			return REQ_VAL_LEN_NOT_MATCH;
		}
		uint32_t  maxCopyLen = m_blockHead->cellDataSize;
		if (cell->next == 0) {
			maxCopyLen = cell->lengthInCell;
		}
		if (len + maxCopyLen > minBufSize) {
			maxCopyLen = minBufSize - len;
		}
		memcpy(dest, cell->bufData, maxCopyLen);
		dest += maxCopyLen;
		len += maxCopyLen;		
		if (cell->next == 0) {
			break;
		}
		if (len >= minBufSize) {
			break;
		}
		cell = (HashValueCell*)CELL_OFFSET_TO_PTR(cell->next);
	}

	if (len != minBufSize) {
		return REQ_VAL_LEN_NOT_MATCH;
	}
	return REQ_OK;
}

int  HashValueBlock::ReqDeleteValue(uint64_t valueID, uint64_t valueSeq, uint32_t valueLen) {

	logdebug("ReqDeleteValue valueID=%d", valueID);
	if (m_tranContext->status != HBTC_IDLE &&
		m_tranContext->status != HBTC_ACK_USED_VALUE &&
		m_tranContext->status != HBTC_ACK_DELETE_VALUE) {
		//当前事务状态不是终点，目前请求开启新事务，需要回滚当前事务
		rollbackCurTransactionContext();
	}

	HashValueCell* cell = (HashValueCell*)(CELL_OFFSET_TO_PTR(valueID));//(HashValueCell*)((char*)m_blockHead + valueID);
	HashValueCell* head = cell;
	HashValueCell* tailCell = head;
	auto headOffset = valueID;
	//uint64_t tailOffset = valueID;

	if (cell == nullptr) {
		return REQ_VAL_ID_NOT_EXITED;
	}
	if (head->cellStatus != HVCS_SETED_OK) {
		//不是首节点
		return REQ_VALUE_NOT_EXISTED;
	}
	if (head->ValueSeq != valueSeq) {
		//校验seq不一致
		return REQ_VALUE_SEQ_NOT_MATCH;
	}
	if (head->lengthInCell != valueLen) {
		//校验长度不一致
		return REQ_VALUE_LEN_NOT_MATCH;
	}

	//校验其他节点
	uint32_t  len = m_blockHead->cellDataSize;
	if (len <= m_blockHead->cellDataSize) {
		len = head->lengthInCell;
	}

	cell = CELL_OFFSET_TO_PTR(cell->next);
	while (cell!=nullptr) {
		tailCell = cell;
		if (cell->cellStatus < HVCS_WRITED_VALUE) {
			return REQ_VALUE_HAVE_DELETED;
		}
		if (cell->ValueSeq != valueSeq) {
			return REQ_VALUE_SEQ_NOT_MATCH;
		}
		if (cell->next == 0) {
			//这个是尾节点
			break;
		}
		cell = CELL_OFFSET_TO_PTR(cell->next);
	}
	if (tailCell != head) {
		//校验尾节点
		//tailOffset=CELL_PTR_TO_OFFSET(tailCell);
		if (tailCell->valHead.next != headOffset) {
			return REQ_LEN_NOT_MATCH;
		}
	}

	//校验一致可以删除
	m_tranContext->m_curOpCell2 = head;
	m_tranContext->m_curOpCell = head;
	m_tranContext->status = HBTC_DELETE_VALUE;
	s_offset_list_t* valList = &(head->valHead);

   //待删除的节点放入空闲链表
	setCellReleasedStatus(head);
	if (tailCell != head) {
		setCellReleasedStatus(tailCell);
	}
	tailCell->next = m_tranContext->freeCellList.next;
	valList->next = 0;
	m_tranContext->freeCellList.next = headOffset;

	m_tranContext->m_curOpCell = nullptr;
	m_tranContext->m_curOpCell2 = nullptr;
	m_tranContext->status = HBTC_ACK_DELETE_VALUE;

	loginfo("ReqDeleteValue REQ_OK");
	return REQ_OK;
}

void HashValueBlock::handleAddPreAllocCellToReleaseList() {

	HashValueCell* cell = m_tranContext->m_curOpCell;
	if (cell != nullptr) {
		auto offset = CELL_PTR_TO_OFFSET(cell);
		cell->ValueSeq = 0;
		cell->lengthInCell = 0;
		cell->cellStatus = HVCS_RELEASED;
		
		if (m_tranContext->allocedList.next == offset) {
			s_offset_list_remove_head(&(m_tranContext->allocedList), this, offset_to_ptr, ptr_to_offset);
		}
		if (m_tranContext->freeCellList.next != offset) {
			s_offset_list_insert_head(&(m_tranContext->freeCellList), (s_offset_list_t*)cell, this, offset_to_ptr, ptr_to_offset);
		}
		m_tranContext->m_curOpCell = nullptr;
	}
	//2:处理剩下的
	s_offset_list_t* head = (s_offset_list_t*)s_offset_list_get_head(&(m_tranContext->allocedList), this, offset_to_ptr, ptr_to_offset);
	while (head != nullptr) {
		cell = (HashValueCell*)head;
		//auto offset = CELL_PTR_TO_OFFSET(cell);
		m_tranContext->m_curOpCell = cell;
		cell->ValueSeq = 0;
		cell->lengthInCell = 0;
		cell->cellStatus = HVCS_RELEASED;
		
		s_offset_list_remove_head(&(m_tranContext->allocedList), this, offset_to_ptr, ptr_to_offset);
		s_offset_list_insert_head(&(m_tranContext->freeCellList), (s_offset_list_t*)cell, this, offset_to_ptr, ptr_to_offset);
		m_tranContext->m_curOpCell = nullptr;
	}
}

void  HashValueBlock::onStatusIdele() {
	//do nothing
}
void HashValueBlock::onStatusAddIdleCellToFreeList() {
	if (m_tranContext->m_curOpCell == nullptr) {
		return;
	}
	HashValueCell* cell = m_tranContext->m_curOpCell;
	auto offset = CELL_PTR_TO_OFFSET(cell);
	auto index = CELL_PTR_TO_INDEX(cell);
	cell->ValueSeq = 0;
	cell->lengthInCell = 0;
	cell->cellStatus = HVCS_RELEASED;
	cell->next = 0;

	if (m_tranContext->freeCellList.next != offset) {
		s_offset_list_insert_head(&(m_tranContext->freeCellList), (s_offset_list_t*)cell, this, offset_to_ptr, ptr_to_offset);
	}
	if (m_tranContext->firstTimeAllocCellIndex <= index) {
		m_tranContext->firstTimeAllocCellIndex = index + 1;
	}
	m_tranContext->m_curOpCell = nullptr;
	m_tranContext->status = HBTC_IDLE;
}
void HashValueBlock::onStatusAddReleasedCellToPreAllocList() {
	//放弃上次启动的事务执行，回滚到初始点
	HashValueCell* cell = m_tranContext->m_curOpCell;
	if (cell != nullptr) {
		auto offset = CELL_PTR_TO_OFFSET(cell);
		cell->ValueSeq = 0;
		cell->lengthInCell = 0;
		cell->cellStatus = HVCS_RELEASED;

		if (m_tranContext->allocedList.next == offset) {
			//还在链表中没移除
			s_offset_list_remove_head(&(m_tranContext->allocedList), this, offset_to_ptr, ptr_to_offset);
		}
		if (m_tranContext->freeCellList.next != offset) {
			//上次没插入进去
			s_offset_list_insert_head(&(m_tranContext->freeCellList), (s_offset_list_t*)cell, this, offset_to_ptr, ptr_to_offset);
		}
		m_tranContext->m_curOpCell = nullptr;
	}

	//2:处理剩下的
	s_offset_list_t* head = (s_offset_list_t*)s_offset_list_get_head(&(m_tranContext->allocedList), this, offset_to_ptr, ptr_to_offset);
	while (head != nullptr) {
		cell = (HashValueCell*)head;
		//auto offset = CELL_PTR_TO_OFFSET(cell);
		m_tranContext->m_curOpCell = cell;
		cell->ValueSeq = 0;
		cell->lengthInCell = 0;
		cell->cellStatus = HVCS_RELEASED;
	
		s_offset_list_remove_head(&(m_tranContext->allocedList), this, offset_to_ptr, ptr_to_offset);
		s_offset_list_insert_head(&(m_tranContext->freeCellList), (s_offset_list_t*)cell, this, offset_to_ptr, ptr_to_offset);
		m_tranContext->m_curOpCell = nullptr;
		head = (s_offset_list_t*)s_offset_list_get_head(&(m_tranContext->allocedList), this, offset_to_ptr, ptr_to_offset);
	}
	m_tranContext->m_curOpCell = nullptr;
	m_tranContext->status = HBTC_IDLE;
}
void HashValueBlock::onStatusAddPreAllocCellToReleaseList() {
	//放弃上次启动的事务执行，回滚到初始点
	handleAddPreAllocCellToReleaseList();
	m_tranContext->status = HBTC_IDLE;
}
void HashValueBlock::onStatusWaitWriteValue() {
	//放弃上次启动的事务执行，回滚到初始点
	handleAddPreAllocCellToReleaseList();
	m_tranContext->status = HBTC_IDLE;
}
void HashValueBlock::onStatusWaitWriteAck() {
	////放弃上次启动的事务执行，回滚到初始点

	if (m_tranContext->m_curOpCell2==nullptr) {
		handleAddPreAllocCellToReleaseList();
		m_tranContext->status = HBTC_IDLE;
	}
	HashValueCell*   headCell= m_tranContext->m_curOpCell2;
	s_offset_list_t* valList = &(headCell->valHead);

	//把这些节点释放回
	HashValueCell* cell = m_tranContext->m_curOpCell;
	if (cell != nullptr) {
		auto offset = CELL_PTR_TO_OFFSET(cell);
		cell->ValueSeq = 0;
		cell->lengthInCell = 0;
		cell->cellStatus = HVCS_RELEASED;
		

		if (valList->next == offset) {
			s_offset_list_remove_head(valList, this, offset_to_ptr, ptr_to_offset);
		}
		if (m_tranContext->freeCellList.next != offset) {
			s_offset_list_insert_head(&(m_tranContext->freeCellList), (s_offset_list_t*)cell, this, offset_to_ptr, ptr_to_offset);
		}
		m_tranContext->m_curOpCell = nullptr;
	}
	//2:处理剩下的
	s_offset_list_t* head = (s_offset_list_t*)s_offset_list_get_head(valList, this, offset_to_ptr, ptr_to_offset);
	while (head != nullptr) {
		cell = (HashValueCell*)head;
		//auto offset = CELL_PTR_TO_OFFSET(cell);
		m_tranContext->m_curOpCell = cell;
		cell->ValueSeq = 0;
		cell->lengthInCell = 0;
		cell->cellStatus = HVCS_RELEASED;
	

		s_offset_list_remove_head(valList, this, offset_to_ptr, ptr_to_offset);
		s_offset_list_insert_head(&(m_tranContext->freeCellList), (s_offset_list_t*)cell, this, offset_to_ptr, ptr_to_offset);
		m_tranContext->m_curOpCell = nullptr;
		head = (s_offset_list_t*)s_offset_list_get_head(valList, this, offset_to_ptr, ptr_to_offset);
	}
	m_tranContext->m_curOpCell = nullptr;
	m_tranContext->m_curOpCell2 = nullptr;
	m_tranContext->status = HBTC_IDLE;
}

void HashValueBlock::onStatusAckUsedValue() {
	//do nothing
}
void HashValueBlock::onStatusDeleteValue() {
	//当前为删除节点的状态，上次没执行完，继续执行
	if (m_tranContext->m_curOpCell2 == nullptr) {
		m_tranContext->status = HBTC_IDLE;
	}
	//m_curOpCell2 存储的是操作的头节点
	//m_curOpCell 存储的是操作的当前节点
	s_offset_list_t* valList = &(m_tranContext->m_curOpCell2->valHead);

	//1:先处理当前节点
	HashValueCell* cell = m_tranContext->m_curOpCell;
	if (cell != nullptr) {
		auto offset = CELL_PTR_TO_OFFSET(cell);
		cell->ValueSeq = 0;
		cell->lengthInCell = 0;
		cell->cellStatus = HVCS_RELEASED;
		

		if (valList->next == offset) {
			s_offset_list_remove_head(valList, this, offset_to_ptr, ptr_to_offset);
		}
		if (m_tranContext->freeCellList.next != offset) {
			s_offset_list_insert_head(&(m_tranContext->freeCellList), (s_offset_list_t*)cell, this, offset_to_ptr, ptr_to_offset);
		}
		m_tranContext->m_curOpCell = nullptr;
	}
	//2:处理剩下的
	s_offset_list_t* head = (s_offset_list_t*)s_offset_list_get_head(valList, this, offset_to_ptr, ptr_to_offset);
	while (head != nullptr) {
		cell = (HashValueCell*)head;
		//auto offset = CELL_PTR_TO_OFFSET(cell);
		m_tranContext->m_curOpCell = cell;
		cell->ValueSeq = 0;
		cell->lengthInCell = 0;
		cell->cellStatus = HVCS_RELEASED;
	

		s_offset_list_remove_head(valList, this, offset_to_ptr, ptr_to_offset);
		s_offset_list_insert_head(&(m_tranContext->freeCellList), (s_offset_list_t*)cell, this, offset_to_ptr, ptr_to_offset);
		m_tranContext->m_curOpCell = nullptr;
		head= (s_offset_list_t*)s_offset_list_get_head(valList, this, offset_to_ptr, ptr_to_offset);
	}
	m_tranContext->m_curOpCell = nullptr;
	m_tranContext->m_curOpCell2 = nullptr;
	m_tranContext->status = HBTC_IDLE;
}
void HashValueBlock::onStatusAckDeleteValue() {
	//do nothing
	
}
//回滾當前事務
void HashValueBlock::rollbackCurTransactionContext() {
	
	switch (m_tranContext->status) {
	case HBTC_IDLE:
		onStatusIdele();
		break;
	//----------------------------
	case HBTC_addIdleCellToFreeList:
		onStatusAddIdleCellToFreeList();
		break;
	case HBTC_addReleasedCellToPreAllocList:
		onStatusAddReleasedCellToPreAllocList();
		break;
	case HBTC_addPreAllocCellToReleaseList:
		onStatusAddPreAllocCellToReleaseList();
		break;
	case HBTC_WAIT_WRITE_VALUE:
		onStatusWaitWriteValue();
		break;
	case HBTC_WAIT_WRITE_ACK:
		onStatusWaitWriteAck();
		break;
	case HBTC_ACK_USED_VALUE:
		onStatusAckUsedValue();
		break;
	//-----------------------------------
	case HBTC_DELETE_VALUE:
		onStatusDeleteValue();
		break;
	case HBTC_ACK_DELETE_VALUE:
		onStatusAckDeleteValue();
		break;
	}

	if (m_tranContext->status != HBTC_IDLE &&
		m_tranContext->status != HBTC_ACK_USED_VALUE &&
		m_tranContext->status != HBTC_ACK_DELETE_VALUE) {
		//不是事务终结点
		logerror("handleCurTransactionContext m_tranContext cur status error:%d", m_tranContext->status);
		return;
	}
}


