#ifndef  _wx_log_h_
#define  _wx_log_h_

#ifdef __cplusplus
extern "C" {
#endif


#include <stdio.h>
#include <stdlib.h>

int   wx_get_log_level();
void  wx_set_log_level(int level);
void  wx_log_init(int level,bool log_to_concole,bool log_to_file);
void  wx_set_logfilepath(const char* path);
void  wx_set_logtoconsole(int tag);


void  wx_log(int loglevel,int forceLogOut,const  char* codeFileName,const char* funcname,int line,const  char* logFileName, const char* prompt,...);

//C99��κ�
#define LogFatal(...)    wx_log(5,0, __FILE__,__FUNCTION__,__LINE__,__FILE__,__VA_ARGS__)
#define logfatal(...)    wx_log(5,0, __FILE__,__FUNCTION__,__LINE__,__FILE__,__VA_ARGS__)

#define LogError(...)    wx_log(4,0, __FILE__,__FUNCTION__,__LINE__,__FILE__,__VA_ARGS__)
#define logerror(...)    wx_log(4,0, __FILE__,__FUNCTION__,__LINE__,__FILE__,__VA_ARGS__)

#define LogWarn(...)     wx_log(3,0,__FILE__,__FUNCTION__,__LINE__, __FILE__,__VA_ARGS__)
#define logwarn(...)     wx_log(3,0,__FILE__,__FUNCTION__,__LINE__, __FILE__,__VA_ARGS__)

#define LogInfo(...)     wx_log(2,0,__FILE__,__FUNCTION__,__LINE__, __FILE__,__VA_ARGS__)
#define loginfo(...)     wx_log(2,0,__FILE__,__FUNCTION__,__LINE__, __FILE__,__VA_ARGS__)

#define LogTrace(...)    wx_log(1,0, __FILE__,__FUNCTION__,__LINE__,__FILE__,__VA_ARGS__)
#define logtrace(...)    wx_log(1,0, __FILE__,__FUNCTION__,__LINE__,__FILE__,__VA_ARGS__)

#define LogDebug(...)    wx_log(0,0, __FILE__,__FUNCTION__,__LINE__,__FILE__,__VA_ARGS__)
#define logdebug(...)    wx_log(0,0, __FILE__,__FUNCTION__,__LINE__,__FILE__,__VA_ARGS__)

#define LogFatal2(destlogfile,...)    wx_log(5, 0, __FILE__,__FUNCTION__,__LINE__,destlogfile,__VA_ARGS__)
#define LogError2(destlogfile,...)    wx_log(4, 0, __FILE__,__FUNCTION__,__LINE__,destlogfile,__VA_ARGS__)
#define LogWarn2(destlogfile,...)     wx_log(3, 0, __FILE__,__FUNCTION__,__LINE__,destlogfile,__VA_ARGS__)
#define LogInfo2(destlogfile,...)     wx_log(2, 0, __FILE__,__FUNCTION__,__LINE__,destlogfile,__VA_ARGS__)
#define LogTrace2(destlogfile,...)    wx_log(1, 0, __FILE__,__FUNCTION__,__LINE__,destlogfile,__VA_ARGS__)
#define LogDebug2(destlogfile,...)    wx_log(0, 0, __FILE__,__FUNCTION__,__LINE__,destlogfile,__VA_ARGS__)

#ifdef __cplusplus 
}
#endif



#endif