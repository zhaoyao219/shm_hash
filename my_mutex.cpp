#include "my_mutex.h"
#include <string.h>

//--------------------------------------------------------------------------


CMyMutex::CMyMutex()
{
#ifdef _WIN32
	InitializeCriticalSection(&m_mutex);
#else
	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&m_mutex, &attr);
#endif
}


CMyMutex::~CMyMutex()
{
#ifdef _WIN32
	DeleteCriticalSection(&m_mutex);
#else
	pthread_mutex_destroy(&m_mutex);
#endif
}


void CMyMutex::lock()
{
#ifdef _WIN32
	EnterCriticalSection(&m_mutex);
#else
	pthread_mutex_lock(&m_mutex);
#endif
}


void CMyMutex::unlock()
{
#ifdef _WIN32
	LeaveCriticalSection(&m_mutex);
#else
	pthread_mutex_unlock(&m_mutex);
#endif
}


//--------------------------------------------------------------------------------

void InitMyMutex(MyMutex* p)
{
#ifdef _WIN32
		InitializeCriticalSection(&(p->m_mutex));
#else
	pthread_mutex_init(&(p->m_mutex),NULL);
#endif
	p->maxLockTime=0;
}

void LockMyMutex(MyMutex* p)
{
	/*unsigned  long interval=0;
	gettimeofday(&(p->start), NULL);*/
	
#ifdef _WIN32
	EnterCriticalSection(&(p->m_mutex));
#else
	pthread_mutex_lock(&(p->m_mutex));
#endif
	/*gettimeofday(&(p->end), NULL);
    interval = 1000000*(p->end.tv_sec - p->start.tv_sec) + (p->end.tv_usec - p->start.tv_usec);
	if(p->maxLockTime<interval)
	{
		p->maxLockTime=interval;
		fprintf(stderr,"Lock  exhaust cur max  time [%f]\n",(interval/1000.0));
	}

	if(interval>1000)
	{
        fprintf(stderr,"Lock  exhaust time [%f]\n",(interval/1000.0));
	}*/

}

void UnLockMyMutex(MyMutex* p)
{
#ifdef _WIN32
	LeaveCriticalSection(&(p->m_mutex));
#else
	pthread_mutex_unlock(&(p->m_mutex));
#endif
}
void DestroyMyMutex(MyMutex* p)
{
#ifdef _WIN32
	DeleteCriticalSection(&(p->m_mutex));
#else
	pthread_mutex_destroy(&(p->m_mutex));
#endif

}



int  MyInitEvent(MyEvent*  pMyEvnet,
				 void*  lpEventAttributes,
				 unsigned  char  bManualReset,
				 unsigned  char  bInitialState,
				 const char*  lpName)
				 
{


#ifdef WIN32
	pMyEvnet->m_bManualReset=bManualReset;
	pMyEvnet->m_bSignal=bInitialState;

	pMyEvnet->m_hEvent=CreateEvent(NULL,pMyEvnet->m_bManualReset,pMyEvnet->m_bSignal,NULL);
	if(pMyEvnet->m_hEvent!=NULL)
	{
		return 0;
	}
	else
	{
		return -1;
	}
#else
	pMyEvnet->m_bSignal=0;
	pthread_mutex_init(&(pMyEvnet->m_mutex),NULL);
	pthread_cond_init(&(pMyEvnet->m_cond),NULL);
	pMyEvnet->m_bManualReset=0;
#endif
	return 0;
}


/************************************************************************/
/*
  阻塞等待事件
*/
/************************************************************************/
int  MyWaiteEvent(MyEvent*  pMyEvnet)
{
#ifdef WIN32
	if(pMyEvnet->m_hEvent==NULL)
	{
		return -1;
	}
	int  ret=0;
	while((ret=WaitForSingleObject(pMyEvnet->m_hEvent,INFINITE))!=WAIT_OBJECT_0)
	{
	//	LogWarn  "error ocur[%d] Event handle[%p]",GetLastError(),m_hEvent);

	}
	/*	switch(ret)
	{
	case WAIT_ABANDONED:
	return false;
	break;//error ocur
	case WAIT_OBJECT_0:
	//The state of the specified object is signaled
	return true;
	break;
	case WAIT_TIMEOUT:
	return true;
	break;
	}*/
	return 1;
#else
	//LogDebug  "begin pthread_mutex_lock    ");
	pthread_mutex_lock(& (pMyEvnet->m_mutex));
	//LogDebug  "pthread_mutex_lock return     ");
	while(! (pMyEvnet->m_bSignal))
	{
	//	LogDebug  "begin pthread_cond_wait      ");
		pthread_cond_wait(& (pMyEvnet->m_cond),&(pMyEvnet->m_mutex));
		//LogDebug  "pthread_cond_wait return      ");
	}

	if(! (pMyEvnet->m_bManualReset))//自动重置信号为非信号状态,注意的是如果是自动重置信号，不能有多个线程等待事件,被一个线程等待后，事件变为非信号了
	{
		pMyEvnet->m_bSignal=0;
	}
	pthread_cond_broadcast( & (pMyEvnet->m_cond));
	pthread_mutex_unlock(& (pMyEvnet->m_mutex));
	return 0;
#endif

}

/************************************************************************/
/*
  超时等待消息
*/
/************************************************************************/
int  MyWaiteTimeOut(MyEvent*  pMyEvnet,unsigned int second,unsigned int millisecond)
{
	//unsigned long timeMillisecond=second*1000+millisecond;
#ifdef WIN32
	if(pMyEvnet->m_hEvent==NULL)
	{
		return -1;
	}
	while(1)
	{
		int  ret=WaitForSingleObject(pMyEvnet->m_hEvent,timeMillisecond);
		if(ret==WAIT_OBJECT_0)
		{
			//等待到事件
			return WAIT_OBJECT_0;
		}
		else if(ret==WAIT_TIMEOUT)
		{
			//超时
			return WAIT_TIMEOUT;
		}

	}
	return 0;
#else
	struct timespec  waittime1={second,millisecond*1000};

	pthread_mutex_lock(& (pMyEvnet->m_mutex));
	while(! (pMyEvnet->m_bSignal))
	{
		pthread_cond_timedwait(&(pMyEvnet->m_cond),&(pMyEvnet->m_mutex),&waittime1);
	}

	if(! (pMyEvnet->m_bManualReset))//自动重置信号为非信号状态,注意的是如果是自动重置信号，不能有多个线程等待事件,被一个线程等待后，事件变为非信号了
	{
		pMyEvnet->m_bSignal=0;
	}
	pthread_cond_broadcast(& (pMyEvnet->m_cond));
	pthread_mutex_unlock(&(pMyEvnet->m_mutex));
	return 0;
#endif

}

/************************************************************************/
/*
 将事件设置为有信号状态
*/
/************************************************************************/
int  MySetEvent(MyEvent*  pMyEvnet)
{
#ifdef _WIN32
	if(pMyEvnet->m_hEvent)
	{
		::SetEvent(pMyEvnet->m_hEvent);
	}
#else
	pthread_mutex_lock(& (pMyEvnet->m_mutex));
	pMyEvnet->m_bSignal=1;
	pthread_cond_broadcast(& (pMyEvnet->m_cond));
	pthread_mutex_unlock(& (pMyEvnet->m_mutex));
#endif

	return 0;


}
/************************************************************************/
/*
  将事件变为无信号状态
*/
/************************************************************************/
void MyResetEvent(MyEvent*  pMyEvnet)
{
#ifdef _WIN32
	if( pMyEvnet->m_hEvent)
	{
		::ResetEvent(pMyEvnet->m_hEvent);
	}
#else
	pthread_mutex_lock(& (pMyEvnet->m_mutex));
	pMyEvnet->m_bSignal=0;
	pthread_mutex_unlock(& (pMyEvnet->m_mutex));
#endif
}

/************************************************************************/
/*
  销毁事件
*/
/************************************************************************/
void MyDestroyEvent(MyEvent*  pMyEvnet)
{
#ifdef _WIN32
	if(pMyEvnet->m_hEvent)
	{
		CloseHandle(pMyEvnet->m_hEvent);
	}
#else
	pthread_cond_destroy(& (pMyEvnet->m_cond));
	pthread_mutex_destroy(& (pMyEvnet->m_mutex));
#endif
}


//----------------------------------------------------
MySimpleEventQueue::MySimpleEventQueue()
{
		InitMyMutex(&m_mutex);
		MyInitEvent(&m_event,NULL,0,0,NULL);
	
}
MySimpleEventQueue::~MySimpleEventQueue()
{
	DestroyMyMutex(&m_mutex);
	MyDestroyEvent(&m_event);
}
unsigned  char*  MySimpleEventQueue::GetMsg(unsigned int&  queueSize)
{
	queueSize=0;
	unsigned char*   msg=NULL;
	LockMyMutex(&m_mutex);
	
	if(m_eventQueue.size()>0)
	{
		msg=m_eventQueue.front();
		m_eventQueue.pop();
		queueSize=m_eventQueue.size();
		UnLockMyMutex(&m_mutex);
		
		return msg;
	}
	else
	{
		UnLockMyMutex(&m_mutex);
		//----------------------------------------------
		while (1)
		{
			msg=NULL;
			MyWaiteEvent(&m_event);
			LockMyMutex(&m_mutex);
			if(m_eventQueue.size()>0)
			{
				msg=m_eventQueue.front();
				m_eventQueue.pop();
				queueSize=m_eventQueue.size();
				UnLockMyMutex(&m_mutex);
				return msg;
			}
			else
			{
				UnLockMyMutex(&m_mutex);
			}
		}
		//------------------------------------------
	}
	return NULL;
}

void    MySimpleEventQueue::PushMsg(unsigned  char* msg)
{
	LockMyMutex(&m_mutex);
	m_eventQueue.push(msg);
	UnLockMyMutex(&m_mutex);
	MySetEvent(&m_event);

}
/************************************************************************/
/*
 清空队列，返回清空的个数
*/
/************************************************************************/
int       MySimpleEventQueue::Clear(int isDeleteMsg)
{
	
		LockMyMutex(&m_mutex);
		int size=m_eventQueue.size();
         
		while(m_eventQueue.size()>0)
		{
			unsigned char * msg=m_eventQueue.front();
			m_eventQueue.pop();
			if(isDeleteMsg)
			{
				delete msg;
			}
		}
		
		UnLockMyMutex(&m_mutex);

		return size;
		
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

MySimpleMsgEventQueue::MySimpleMsgEventQueue()
{
	InitMyMutex(&m_mutex);
	MyInitEvent(&m_event,NULL,0,0,NULL);

	//--------------------------------------------
	m_simpleMsgEventQueuePtr=NULL;
	InitMyMutex(&m_backMutex);
	m_pBackEventQueue=&m_eventQueue1;

	InitMyMutex(&m_frontMutex);
	m_pFrontEventQueue=&m_eventQueue2;

	//------------------------------------
}

MySimpleMsgEventQueue::~MySimpleMsgEventQueue()
{
	DestroyMyMutex(&m_mutex);
	MyDestroyEvent(&m_event);

}
/************************************************************************/
/*
 返回消息队列中剩下的消息数
*/
/************************************************************************/
unsigned  int    MySimpleMsgEventQueue::GetMsg(MySimpleMsg*  pOutMsg)
{
	memset((void*) pOutMsg,0,sizeof(MySimpleMsg) );

	LockMyMutex(&m_mutex);
	unsigned  int esize=m_eventQueue.size();
	if(esize>0)
	{
		 const MySimpleMsg& 	outMsg=m_eventQueue.front();
		memcpy((void*)pOutMsg,(void*)&outMsg,sizeof(MySimpleMsg));
		m_eventQueue.pop();
		UnLockMyMutex(&m_mutex);
		return (esize-1);
		
	}
	else
	{
		UnLockMyMutex(&m_mutex);
		//----------------------------------------------
		while (1)
		{
			MyWaiteEvent(&m_event);
			LockMyMutex(&m_mutex);
			unsigned  int esize=m_eventQueue.size();
			if(m_eventQueue.size()>0)
			{
				const MySimpleMsg& 	outMsg=m_eventQueue.front();
				memcpy((void*)pOutMsg,(void*)&outMsg,sizeof(MySimpleMsg));
				m_eventQueue.pop();
				UnLockMyMutex(&m_mutex);
			   return (esize-1);
			}
			else
			{
				UnLockMyMutex(&m_mutex);
			}
		}
		//------------------------------------------
	}


}
void  MySimpleMsgEventQueue::PushMsg(MySimpleMsg*  msg)
{

	LockMyMutex(&m_mutex);
	m_eventQueue.push(*msg);
	UnLockMyMutex(&m_mutex);
	MySetEvent(&m_event);

}

void    MySimpleMsgEventQueue::PushMsg( unsigned int eventId, unsigned char* msg, unsigned int len)
{
	LockMyMutex(&m_mutex);

	MySimpleMsg  smsg;
	smsg.eventNo=eventId;
	smsg.msg=(unsigned char*)msg;
	smsg.msgLen=len;

	m_eventQueue.push(smsg);
	
	UnLockMyMutex(&m_mutex);
	MySetEvent(&m_event);
}


