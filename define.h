#pragma once


#define MhtHead_MAGIC   "_MhtHead_"
#define MhtTail_MAGIC   "_MhtTail_"
#define MhtHead_VERSION   101


#define HashNodeRowTag  "_ROW_"
//HASH 节点缺省行数
#define  HashNodeRowsDefaultNum  20
#define  HashNodeRowsMinNum  10
#define  HASH_NODE_KEY_DATA_SIZE  250
//hash value的CELL 数 KEY节点数 的倍率
#define  HASH_VALUE_CELL_KEY_NUMBER_RATION  2


#define  HashBlockHead_VERSION_NO  101
#define  HashBlockInitTag  "_initialized_"
#define  HashBlockHead_MAGIC   "_HashBlockHead_"
#define  HashBlockTail_MAGIC   "_HashBlockTail_"

/**
#define  HashBlockMinCellDataSize  256
#define  HashBlockMaxCellDataSize  30000
#define  HashBlockDefaultCellDataSize  512
*/

#define  HashBlockMinCellDataSize  256
#define  HashBlockMaxCellDataSize  512
#define  HashBlockDefaultCellDataSize  512


/**
#define  HashBlockMinCellNum   10000
#define  HashBlockMaxCellNum   3000000
#define  HashBlockDefaultCellNum   30000
*/



enum GetKeyRet {
	//获取成功
	GET_OK,
	//获取不存在
	GET_NOT_EXISYED
};
enum SetKeyRet {
	//设置成功
	SET_OK,
	//已经存在
	SET_KEY_HAS_EXISYED,
	//没有空间插入
	SET_NO_SPACE,
	//没有存值的空间
	SET_NO_VALUE_SPACE,
	//内部错误，错误状态
	SET_ERROR_NODE_STATUS,
	//写值错误
	SET_WRITE_VALUE_ERROR
};
enum RequsetValueRet {
	//读取成功
	REQ_OK,
	//键不存在
	REQ_VAL_ID_NOT_EXITED,
	//键不匹配
	REQ_VAL_ID_NOT_MATCH,
	//长度不匹配
	REQ_VAL_LEN_NOT_MATCH,
	//SEQ 不匹配
	REQ_VAL_SEQ_NOT_MATCH,
	//狀態不匹配
	REQ_VAL_STATUS_NOT_MATCH,
	//当前事务状态不匹配
	REQ_TR_STATUS_NOT_MATCH,
	//请求的长度不匹配
	REQ_LEN_NOT_MATCH,
	//删除的键不存在
	REQ_KEY_NOT_EXISTED,
	//已经被删除
	REQ_KEY_HAVE_DELETED,
	//删除的键值不存在
	REQ_VALUE_NOT_EXISTED,
	REQ_VALUE_SEQ_NOT_MATCH,
	REQ_VALUE_LEN_NOT_MATCH,
	//删除值已经被删除
	REQ_VALUE_HAVE_DELETED,
	//删除的键不存在
	

};



