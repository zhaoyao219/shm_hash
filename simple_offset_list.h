#pragma once

#include  <stdint.h>
#include "wx_log.h"

//基于偏移的简单的单实现,用于共享内存
//只提供受限操作，操作链表经量步骤少，防止进程崩溃，链表断裂
typedef struct s_offset_list_s  s_offset_list_t;

struct s_offset_list_s {
    volatile uint64_t next;
   
};

typedef s_offset_list_t* OFFSET_TO_PTR_FUNC_T(void* context, uint64_t      offset);
typedef uint64_t         PTR_TO_OFFSET_FUNC_T(void* context, s_offset_list_t* node);

//需要实现，把偏移转指针
//offset_list_t* offset_to_ptr(void* context,uint32_t  offset);
//需要实现，把指针转偏移
//uint32_t ptr_to_offset(void* context,offset_list_t*  node);

static inline
void  s_offset_list_init(s_offset_list_t* head) {
    head->next = 0;
}
//向链表上头节点挂，先改待插入节点
static inline
void s_offset_list_insert_head(s_offset_list_t* head, s_offset_list_t*  node, void*  context, 
    OFFSET_TO_PTR_FUNC_T offset_to_ptr, PTR_TO_OFFSET_FUNC_T ptr_to_offset)     {

    // auto next=       offset_to_ptr(context,node->next); 
     auto node_offset=ptr_to_offset(context,node);  
    // auto h_offset=   ptr_to_offset(context,head);  
    (node)->next = (head)->next;    
    (head)->next = node_offset;
 }
static inline
bool s_offset_list_empty(s_offset_list_t* head) {
    return (head->next == 0);
}
static inline
void s_offset_list_clear(s_offset_list_t* head) {
    head->next = 0;
}
static inline
s_offset_list_t*  s_offset_list_get_head(s_offset_list_t*  list,void* context, OFFSET_TO_PTR_FUNC_T offset_to_ptr, PTR_TO_OFFSET_FUNC_T ptr_to_offset)     {
   return  offset_to_ptr(context, list->next) ;
 }
static inline
s_offset_list_t* s_offset_list_get_next(s_offset_list_t* node, void* context,OFFSET_TO_PTR_FUNC_T offset_to_ptr, PTR_TO_OFFSET_FUNC_T ptr_to_offset) {
   return  offset_to_ptr(context, node->next);
}

static inline
s_offset_list_t*  s_offset_list_remove_head(s_offset_list_t* list,void* context, OFFSET_TO_PTR_FUNC_T offset_to_ptr, PTR_TO_OFFSET_FUNC_T ptr_to_offset)     {
    s_offset_list_t*  head=offset_to_ptr(context, list->next);
    if (head != nullptr) {
        list->next = head->next;
    }
    else {
        list->next = 0;
    }
    return head;
 } 

static inline
void s_offset_list_remove_head_next(s_offset_list_t* list, uint64_t  next, void* context, OFFSET_TO_PTR_FUNC_T offset_to_ptr, PTR_TO_OFFSET_FUNC_T ptr_to_offset) {
    list->next = next;
   
}



   


