BASE_PATH=../../

INC_PATH=-I.  -I$(BASE_PATH)

LIBDIR=-L.  

LIB_PATH=$(LIBDIR)   -lpthread     -ldl   



SRCPATH= ./
CPPFILE=$(wildcard $(SRCPATH)*.cpp)
CPPOBJS=$(patsubst %.cpp,%.o,$(patsubst $(SRCPATH)%,$(OBJDIR)%,$(CPPFILE)))
	

CC1=g++
CC=gcc
COMPILE_MACRO=-g -Wall    -std=c++11  -march=x86-64   -Werror  -fPIC 
LINK=gcc
GEN_OPT=-g    -shared -o2  -fPIC   -Werror  -Wall 
#GEN_OPT=-g     -o3 -fPIC -Werror  -Wall 

OBJDIR=./
DBLIBDIR=
LIB_NAME=shmhash.so
       
all:$(CPPOBJS) 
		g++  $(GEN_OPT)  -o  libshmhash.so   $(CPPOBJS)   $(LIB_PATH)
$(OBJDIR)%.o:$(SRCPATH)%.cpp
	$(CC1)  -c  -o3  $(COMPILE_MACRO) $(INC_PATH)  $< -o $@  
clean:
	-rm -rf $(CPPOBJS) $(DBLIBDIR)$(DBLIBNAME)
	rm -rf  shmhash
	rm -rf  core.*
	

		







  
